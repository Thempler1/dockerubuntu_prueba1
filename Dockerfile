FROM ubuntu
RUN mkdir newFolder
RUN cd newFolder/
RUN mkdir txts
RUN cd txts
RUN echo "Soy un txt" > txtGenerico.txt
#RUN mv txtGenerico txtGenericoV2
RUN cd ..
RUN mkdir mds
RUN cd mds/
RUN echo "soy un md" > mdGenerico.md
#RUN mv mdGenerico mdGenericoV2
RUN cd ..
RUN pwd
RUN cd ..
RUN cp -r newFolder/ newFolder1/
RUN cp -r newFolder/ newFolder2/
RUN cp -r newFolder/ newFolder3/
RUN cp -r newFolder/ newFolder4/
RUN cd ..
RUN apt update
#RUN apt install git
#RUN mkdir control2Leyton
#RUN cd control2Leyton/
#RUN git clone https://gitlab.com/Thempler1/scriptbash.git
#RUN cd ..
RUN ls | sort
COPY ./soyUnScript.sh soyUnScript.sh
ADD ./soyUnScript.sh soyUnScript.sh
RUN chmod +x ./soyUnScript.sh
RUN ./soyUnScript.sh
